import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestAssignment02student {

	AVLTree tree;	
	void reset() {
		tree = new AVLTree();
	}

	public boolean insert(Integer key, Float value) {
		return tree.insert(key, value);
	}

	public boolean insert(Integer key) {
		return tree.insert(key, (float) key);
	}

	public boolean remove(Integer key) {
		return tree.remove(key);
	}

	public void setUp() {
		reset();
		insert(Integer.valueOf(5));
		insert(Integer.valueOf(18));
		insert(Integer.valueOf(2));
		insert(Integer.valueOf(8));
		insert(Integer.valueOf(14));
		insert(Integer.valueOf(16));
		insert(Integer.valueOf(13));
		insert(Integer.valueOf(3));
		insert(Integer.valueOf(12));
		insert(Integer.valueOf(21));
		insert(Integer.valueOf(1));
		insert(Integer.valueOf(0));
	}

	@Test
	public void testStructure1RotationHardcoded() {
		reset();
		insert(Integer.valueOf(5));
		insert(Integer.valueOf(18));
		insert(Integer.valueOf(2));
		insert(Integer.valueOf(8));
		insert(Integer.valueOf(14));
		AVLNode root = tree.getRoot();

		// test structure
		assertEquals(root.key, Integer.valueOf(5));
		assertEquals(2, root.height);
		// left branch
		assertEquals(root.left.key, Integer.valueOf(2));
		assertEquals(root.left.height, 0);
		// right branch
		assertEquals(root.right.key, Integer.valueOf(14));
		assertEquals(root.right.height, 1);
		assertEquals(root.left.left, null);
		assertEquals(root.left.right, null);
		assertEquals(root.right.left.key, Integer.valueOf(8));
		assertEquals(root.right.left.height, 0);
		assertEquals(root.right.left.left, null);
		assertEquals(root.right.left.right, null);
		assertEquals(root.right.right.key, Integer.valueOf(18));
		assertEquals(root.right.right.height, 0);
		assertEquals(root.right.right.left, null);
		assertEquals(root.right.right.right, null);
	}

	@Test
	public void testToArray() {
		setUp();
		Float[] array = tree.toArray();

		System.out.println("-------------------------------------------------------");
		System.out.println("testToArray\nOrdered (Pre):   5.0 2.0 1.0 0.0 3.0 14.0 12.0 8.0 13.0 18.0 16.0 21.0");
		System.out.print("Your order:      ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		assertEquals(array[0], Float.valueOf(5.0f));
		assertEquals(array[1], Float.valueOf(2.0f));
		assertEquals(array[2], Float.valueOf(1.0f));
		assertEquals(array[3], Float.valueOf(0.0f));
		assertEquals(array[4], Float.valueOf(3.0f));
		assertEquals(array[5], Float.valueOf(14.0f));
		assertEquals(array[6], Float.valueOf(12.0f));
		assertEquals(array[7], Float.valueOf(8.0f));
		assertEquals(array[8], Float.valueOf(13.0f));
		assertEquals(array[9], Float.valueOf(18.0f));
		assertEquals(array[10], Float.valueOf(16.0f));
		assertEquals(array[11], Float.valueOf(21.0f));

		boolean success = checkAVLIntegrity();
		String s = "";
		if (!success) {
			s = " NOT";
		}
		System.out.println("testToArray: AVL integrity check" + s + " successfull");
		assertTrue(success);

	}

	@Test
	public void testDeleteCase0() {

		setUp();
		Float[] array = tree.toArray();

		System.out.println("-------------------------------------------------------");
		System.out.println("remove key 88");
		System.out.print("Your order (Pre):       ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		boolean succsess = remove(88);
		assertTrue(!succsess);
		Float[] arr = tree.toArray();

		System.out.print("Your order (Post):      ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		boolean success = checkAVLIntegrity();
		String s = "";
		if (!success) {
			s = " NOT";
		}
		System.out.println("testToArray: AVL integrity check" + s + " successfull");
		assertTrue(success);


	}

	@Test
	public void testDeleteCase1() {

		setUp();
		Float[] array = tree.toArray();

		System.out.println("-------------------------------------------------------");
		System.out.println("remove key 0");
		System.out.print("Your order (Pre):       ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		boolean succsess = remove(0);
		assertTrue(succsess);
		Float[] arr = tree.toArray();

		System.out.print("Your order (Post):      ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		boolean success = checkAVLIntegrity();
		String s = "";
		if (!success) {
			s = " NOT";
		}
		System.out.println("testToArray: AVL integrity check" + s + " successfull");
		assertTrue(success);


	}

	@Test
	public void testDeleteCase2() {

		setUp();
		Float[] array = tree.toArray();

		System.out.println("-------------------------------------------------------");
		System.out.println("remove key 2");
		System.out.print("Your order (Pre):       ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		boolean succsess = remove(3);
		assertTrue(succsess);
		Float[] arr = tree.toArray();

		System.out.print("Your order (Post):      ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		boolean success = checkAVLIntegrity();
		String s = "";
		if (!success) {
			s = " NOT";
		}
		System.out.println("testToArray: AVL integrity check" + s + " successfull");
		assertTrue(success);

	}

	@Test
	public void testDeleteCase3() {

		setUp();
		Float[] array = tree.toArray();

		System.out.println("-------------------------------------------------------");
		System.out.println("remove key 18");
		System.out.print("Your order (Pre):       ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		boolean succsess = remove(18);
		assertTrue(succsess);
		Float[] arr = tree.toArray();

		System.out.print("Your order (Post):      ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		boolean success = checkAVLIntegrity();
		String s = "";
		if (!success) {
			s = " NOT";
		}
		System.out.println("testToArray: AVL integrity check" + s + " successfull");
		assertTrue(success);


	}

	private boolean checkAVLIntegrity() {
		return checkAVLIntegrity(tree.getRoot());
	}

	private boolean checkAVLIntegrity(AVLNode n) {
		boolean isAVL = true;
		if (n == null) return true;
		if (!isAVLTree(n)) isAVL = false;
		
		if(!isAVL) {
			int lh = n.left==null ? -1:n.left.height;
			int rh = n.right==null ? -1:n.right.height;
			System.out.println(n.height+ ";"+lh+";" +rh);
		}
		
		if(!checkAVLIntegrity(n.left)) isAVL = false;
		if(!checkAVLIntegrity(n.right)) isAVL = false;
		
		return isAVL;
	}
	
	private boolean isAVLTree(AVLNode n) {
		int diff = (n.left == null ? -1 : n.left.height)
				- (n.right == null ? -1 : n.right.height);
		return -1 <= diff && diff <= 1;
	}

}
