
public class AVLNode {
	public AVLNode parent = null;
	public AVLNode left = null;
	public AVLNode right = null;
	
	public Integer key;
	public Float elem;

	public int height = 0; // To determine node height in O(1)

	public AVLNode(Integer key,  Float elem) {
		this.key = key;
		this.elem = elem;
	}

	public AVLNode(Integer key) {
		this.key = key;
		this.elem = null;
	}

	@Override
	public String toString() {
		return "key:" + key + ", element: " + elem;
	}
}

