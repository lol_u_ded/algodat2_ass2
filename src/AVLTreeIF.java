
public interface AVLTreeIF {
	
	/**
	 * @returns the root of the AVLTree
	 */
	public AVLNode getRoot();
	
	/**
	 * Retrieves tree height.
	 * 
	 * @return -1 in case of empty tree, current tree height otherwise.
	 */
	public int height();
	
	
	/**
	 * Yields number of key/element pairs in the tree.
	 * 
	 * @return Number of key/element pairs.
	 */
	public int size();
	
	
	/**
	 * Yields an array representation of the tree (pre-order).
	 * 
	 * @return Array representation of the tree.
	 */
	public Float[] toArray();
	
	
	/**
	 * Returns element of node with given key.
	 * 
	 * @param key
	 *            Key to search.
	 * @return 	  
	 *            Corresponding element if key was found, null otherwise.
	 * @throws 
	 *            IllegalArgumentException if key is null.
	 */
	public Float find(Integer key) throws IllegalArgumentException;
	
	
	/**
	 * Inserts a new node into AVL tree.
	 * 
	 * @param key
	 *            Key of the new node.
	 * @param elem
	 *            Data of the new node. Elements with the same key
	 *            are not allowed. In this case false is returned. Null-Keys are
	 *            not allowed. In this case an exception is thrown.
	 * @throws 
	 *            IllegalArgumentException if one of the parameters is null.
	 */
	public boolean insert(Integer key, Float elem) throws IllegalArgumentException; 
	
	
	/**
	 * Removes node with given key.
	 * 
	 * @param key
	 *            Key of node to remove.
	 * @return 
	 *            true If element was found and deleted.
	 * @throws 
	 *            IllegalArgumentException if key is null.
	 */
	public boolean remove(Integer key) throws IllegalArgumentException;
	
}
