import java.util.ArrayList;
import java.util.List;

public class AVLTree implements AVLTreeIF {

	private AVLNode root;
	private int size;
	private static final int T0 = 0, A = 1, T1 = 2, B = 3, T2 = 4, C = 5, T3 = 6;
	private ArrayList<Float> floats = null;
	AVLNode[] avlArr = new AVLNode[7];


	/**
	 * Default constructor. Initializes the AVL tree.
	 */
	public AVLTree() {
		root = null;
		size = 0;
	}

	/**
	 * @return the root of the AVLTree
	 */
	public AVLNode getRoot() {
		return root;
	}

	/**
	 * Retrieves tree height.
	 *
	 * @return -1 in case of empty tree, current tree height otherwise.
	 */
	public int height() {
		if (root != null) {
			updateHeit(root);
			return root.height;
		}
		return -1;
	}

	/**
	 * Yields number of key/element pairs in the tree.
	 *
	 * @return Number of key/element pairs.
	 */
	public int size() {
		return size;
	}

	/**
	 * Yields an array representation of the tree elements (pre-order).
	 *
	 * @return Array representation of the tree elements.
	 */
	public Float[] toArray() {
		if (root == null) {
			return null;
		}
		floats = new ArrayList<>();
		recursivePreOrder(root);
		return floats.toArray(new Float[0]);
	}

	// private auxiliary method - preorder
	private void recursivePreOrder(AVLNode node) {
		if (node == null) {
			return;
		}
		floats.add(node.elem);
		recursivePreOrder(node.left);
		recursivePreOrder(node.right);
	}

	/**
	 * Returns element of node with given key.
	 *
	 * @param key
	 *            Key to search.
	 * @return
	 *            Corresponding element if key was found, null otherwise.
	 * @throws
	 *            IllegalArgumentException if key is null.
	 */
	public Float find(Integer key) throws IllegalArgumentException {
		if (key == null) {
			throw new IllegalArgumentException();
		}

		AVLNode searched = findKey(key, root);

		if (searched != null) {
			return searched.elem;
		}

		return null;
	}

	// private auxiliary method - find key
	private AVLNode findKey(Integer key, AVLNode avlNode) {
		// reached end of current branch
		if (avlNode == null) {
			return null;
		}
		// found
		if (avlNode.key.equals(key)) {
			return avlNode;
		}
		// next recursion step
		if (key < avlNode.key) {
			return findKey(key, avlNode.left);
		} else {
			return findKey(key, avlNode.right);
		}
	}

	/**
	 * Inserts a new node into AVL tree.
	 *
	 * @param key
	 *            Key of the new node.
	 * @param elem
	 *            Data of the new node. Elements with the same key
	 *            are not allowed. In this case false is returned. Null-Keys are
	 *            not allowed. In this case an exception is thrown.
	 * @throws
	 *            IllegalArgumentException if one of the parameters is null.
	 */
	public boolean insert(Integer key, Float elem) throws IllegalArgumentException {
		if (key == null || elem == null) {
			throw new IllegalArgumentException();
		}
		if (findKey(key, root) != null) {
			/* if there  are double keys */
			return false;
		}
		AVLNode newAvlNode = new AVLNode(key, elem);
		if (root == null) /* empty tree */ {
			root = newAvlNode;
			size++;
			return true;
		}
		insert(root, newAvlNode);
		updateHeit(newAvlNode);
		checkBranch(newAvlNode);
		return true;
	}

	// private auxiliary method - reconstructs
	private AVLNode reconstruct(AVLNode curr) {
		if (curr == null || curr.parent == null || curr.parent.parent == null) {
			throw new IllegalArgumentException(); // check Error-cases
		}

		AVLNode z = curr.parent.parent;
		AVLNode y = curr.parent;
		AVLNode x = curr;                // redundant but good for readability

		AVLNode sol = curr;
		if (z.key > y.key && z.key > x.key && y.key > x.key) {
			sol = doRightRotation(z, y, x);
		} else if (x.key > y.key && x.key > z.key && y.key > z.key) {
			sol = doLeftRotation(z, y, x);
		} else if (y.key > z.key && y.key > x.key && x.key > z.key) {
			sol = doRightLeftRotation(z, y, x);
		} else if (z.key > y.key && x.key > y.key && z.key > x.key) {
			sol = doLeftRightRotation(z, y, x);
		}
		return sol;
	}

	// private auxiliary method - Case1 Single Left Rotation
	private AVLNode doLeftRotation(AVLNode z, AVLNode y, AVLNode x) {
		avlArr[A] = z;
		avlArr[B] = y;
		avlArr[C] = x;
		avlArr[T0] = z.left;
		avlArr[T1] = y.left;
		avlArr[T2] = x.left;
		avlArr[T3] = x.right;

		if (avlArr[A] == root) {
			root = avlArr[B];
		} else {
			if (avlArr[A].parent.left == avlArr[A]) {
				avlArr[A].parent.left = avlArr[B];
			} else {
				avlArr[A].parent.right = avlArr[B];
			}
		}

		avlArr[B].parent = avlArr[A].parent;

		return rotate();


	}

	// private auxiliary method - Case2 Single Right Rotation
	private AVLNode doRightRotation(AVLNode z, AVLNode y, AVLNode x) {
		avlArr[A] = x;
		avlArr[B] = y;
		avlArr[C] = z;
		avlArr[T0] = x.left;
		avlArr[T1] = x.right;
		avlArr[T2] = y.right;
		avlArr[T3] = z.right;

		if (avlArr[C] == root) {
			root = avlArr[B];
		} else {
			if (avlArr[C].parent.left == avlArr[C]) {
				avlArr[C].parent.left = avlArr[B];
			} else {
				avlArr[C].parent.right = avlArr[B];
			}
		}
		avlArr[B].parent = avlArr[C].parent;

		return rotate();
	}

	// private auxiliary method - Case3 Double Right-Left Rotation
	private AVLNode doRightLeftRotation(AVLNode z, AVLNode y, AVLNode x) {
		avlArr[A] = z;
		avlArr[B] = x;
		avlArr[C] = y;
		avlArr[T0] = z.left;
		avlArr[T1] = x.left;
		avlArr[T2] = x.right;
		avlArr[T3] = y.right;

		if (avlArr[A] == root) {
			root = avlArr[B];
		} else {
			if (avlArr[A].parent.left == avlArr[A]) {
				avlArr[A].parent.left = avlArr[B];
			} else {
				avlArr[A].parent.right = avlArr[B];
			}
		}
		avlArr[B].parent = avlArr[A].parent;

		return rotate();
	}

	// private auxiliary method - Case4 Double Left-Right Rotation
	private AVLNode doLeftRightRotation(AVLNode z, AVLNode y, AVLNode x) {
		avlArr[A] = y;
		avlArr[B] = x;
		avlArr[C] = z;
		avlArr[T0] = y.left;
		avlArr[T1] = x.left;
		avlArr[T2] = x.right;
		avlArr[T3] = z.right;

		if (avlArr[C] == root) {
			root = avlArr[B];
		} else {
			if (avlArr[C].parent.left == avlArr[C]) {
				avlArr[C].parent.left = avlArr[B];
			} else {
				avlArr[C].parent.right = avlArr[B];
			}
		}
		avlArr[B].parent = avlArr[C].parent;

		return rotate();
	}

	// private auxiliary method - the Rotation
	private AVLNode rotate() {

		avlArr[B].left = avlArr[A];
		avlArr[A].parent = avlArr[B];

		avlArr[B].right = avlArr[C];
		avlArr[C].parent = avlArr[B];

		if (avlArr[T0] != null) {
			avlArr[T0].parent = avlArr[A];
			avlArr[A].left = avlArr[T0];
		} else {
			avlArr[A].left = null;
		}

		if (avlArr[T1] != null) {
			avlArr[T1].parent = avlArr[A];
			avlArr[A].right = avlArr[T1];
		} else {
			avlArr[A].right = null;
		}

		if (avlArr[T2] != null) {
			avlArr[T2].parent = avlArr[C];
			avlArr[C].left = avlArr[T2];
		} else {
			avlArr[C].left = null;
		}

		if (avlArr[T3] != null) {
			avlArr[T3].parent = avlArr[C];
			avlArr[C].right = avlArr[T3];
		} else {
			avlArr[C].right = null;
		}

		avlArr[A].height = Math.max(getHeight(avlArr[A].right), getHeight(avlArr[A].left)) + 1;
		avlArr[C].height = Math.max(getHeight(avlArr[C].right), getHeight(avlArr[C].left)) + 1;
		AVLNode par = avlArr[B];
		while (par != null) {
			par.height = Math.max(getHeight(par.right), getHeight(par.left)) + 1;
			par = par.parent;
		}


		AVLNode checkC = avlArr[C];
		AVLNode checkB = avlArr[B];
		AVLNode checkA = avlArr[A];
		avlArr = new AVLNode[7];

		if (checkA.parent.parent != null && Math.abs(getBalance(checkA)) > 1) {
			return reconstruct(checkA);
		} else if (checkC.parent.parent != null && Math.abs(getBalance(checkC)) > 1) {
			return reconstruct(checkC);
		} else if (checkB.parent != null && checkB.parent.parent != null && Math.abs(getBalance(checkB.parent)) > 1) {
			return reconstruct(checkB.parent);
		} else if (Math.abs(getBalance(checkB)) > 1) {
			if (checkC.height > checkA.height) {
				if (checkC.left != null) {
					return reconstruct(checkC.left);
				} else/* checkC.right != null */ {
					return reconstruct(checkC.right);
				}
			} else/* checkC.height < checkA.height */ {
				if (checkA.left != null) {
					return reconstruct(checkA.left);
				} else/* checkA.right != null */ {
					return reconstruct(checkA.right);
				}
			}
		}

		return checkB;

	}

	// private auxiliary method - increment heit from given node and go up
	private void updateHeit(AVLNode curr) {
		while (curr != null) {
			curr.height = Math.max(getHeight(curr.left), getHeight(curr.right)) + 1;
			curr = curr.parent;
		}
	}

	// private auxiliary method - get curr heit
	private int getHeight(AVLNode curr) {
		if (curr != null) {
			return curr.height;
		}
		return -1;
	}

	// private auxiliary method - insert
	private boolean insert(AVLNode curr, AVLNode newAvlNode) {
		boolean flag;
		// add left key < curr.key
		if (newAvlNode.key < curr.key) {
			if (curr.left != null) {
				flag = insert(curr.left, newAvlNode);
			} else {
				curr.left = newAvlNode;
				newAvlNode.parent = curr;
				size++;
				return true;
			}
			// add right key > curr.key
		} else if (newAvlNode.key > curr.key) {
			if (curr.right != null) {
				flag = insert(curr.right, newAvlNode);
			} else {
				curr.right = newAvlNode;
				newAvlNode.parent = curr;
				size++;
				return true;
			}
		} else /*(right key == curr.key) */ {
			// Error should not be reached.
			flag = false;
		}
		return flag;
	}

	// private auxiliary method - balance curr to right - left, may be negative
	private int getBalance(AVLNode avlNode) {
		return getHeight(avlNode.right) - getHeight(avlNode.left);
	}

	/**
	 * Removes node with given key.
	 *
	 * @param key Key of node to remove.
	 * @return true If element was found and deleted.
	 * @throws IllegalArgumentException if key is null.
	 */
	public boolean remove(Integer key) throws IllegalArgumentException {
		if (key == null) {
			throw new IllegalArgumentException();
		}

		AVLNode deleteMe = findKey(key, root);

		// if key is not present return
		if (deleteMe == null) {
			return false;

		} else /* if key is present search and delete */ {

			if (deleteMe == getRoot()) {
				return specialCase(deleteMe);

			} else {
				AVLNode parent = deleteMe.parent;

				// Case1 node has no children thus it can be removed
				if (deleteMe.left == null && deleteMe.right == null) {
					// find side to be set null
					if (parent.left == deleteMe) {
						parent.left = null;
					} else if (parent.right == deleteMe) {
						parent.right = null;
					}
					size--;
					updateHeit(parent);
					checkBranch(parent);
					return true;
				}

				// Case2 node has only one child node
				else if (deleteMe.left == null || deleteMe.right == null) {

					if (deleteMe.left == null) /* deleteMe.right != null */ {
						if (parent.left == deleteMe) {
							parent.left = deleteMe.right;
						} else if (parent.right == deleteMe) {
							parent.right = deleteMe.right;
						}
						deleteMe.right.parent = parent;
						updateHeit(parent.right);
						checkBranch(parent.right);

					} else /* deleteMe.left != null */ {
						if (parent.left == deleteMe) {
							parent.left = deleteMe.left;
						} else if (parent.right == deleteMe) {
							parent.right = deleteMe.left;
						}
						deleteMe.left.parent = parent;
						updateHeit(parent.left);
						checkBranch(parent.left);
					}
					size--;
					return true;
				}

				// Case3 node has two child nodes
				else {
					return specialCase(deleteMe);
				}
			}
		}
	}

	// private auxiliary method - Checks if an rotation is needed
	private void checkBranch(AVLNode curr) {
		while (curr.parent != null && curr.parent.parent != null) {
			if (Math.abs(getBalance(curr.parent.parent)) > 1) {
				curr = reconstruct(curr);
			} else {
				curr = curr.parent;
			}
		}
	}

	// private auxiliary method - special Case of remove function
	private boolean specialCase(AVLNode deleteMe) {
		AVLNode parent = deleteMe.parent;
		List<AVLNode> nodes = getAllNodes(deleteMe);

		for (AVLNode n : nodes) {
			if (n != deleteMe) {
				insert(n.key, n.elem);
			} else {
				if (parent.left == deleteMe) {
					parent.left = null;
				} else /* if(parent.right == deleteMe) */ {
					parent.right = null;
				}
			}
		}
		size--;
		updateHeit(parent);
		checkBranch(parent);
		return true;
	}

	// private auxiliary method - adds all nodes to a list
	private ArrayList<AVLNode> getAllNodes(AVLNode curr) {
		ArrayList<AVLNode> nodes = new ArrayList<>();
		if (curr != null) {
			nodes.add(curr);
			nodes.addAll(getAllNodes(curr.left));
			nodes.addAll(getAllNodes(curr.right));
			return nodes;
		}
		return nodes;
	}

}
