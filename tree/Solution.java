class Solution {
	public TreeNode insertAVL(int[] items, int threshold) {
		if (items == null || items.length == 0) {
			return null;
		}

		AVLNode root = new AVLNode(items[0]);

		for (int idx = 1; idx < items.length; idx++) {
			root = insert(root, items[idx], threshold);
		}

		return convertAvlNodesToTreenode(root);
	}

	private AVLNode insert(AVLNode node, int key, int threshold) {
		if (node == null) {
			return new AVLNode(key);
		}

		if (key < node.val) {
			node.left = insert(node.left, key, threshold);
		} else {
			node.right = insert(node.right, key, threshold);
		}

		node.height = 1 + Math.max(
				getHeight(node.left),
				getHeight(node.right)
		);
		int balance = getBalance(node);

		if (balance > threshold) {
			if (getBalance(node.left) >= 0) {
				node = rotateRight(node);
			} else {
				node = rotateLeftRight(node);
			}
		} else if (balance < -threshold) {
			if (getBalance(node.right) <= 0) {
				node = rotateLeft(node);
			} else {
				node = rotateRightLeft(node);
			}
		}

		return node;
	}

	private AVLNode rotateRight(AVLNode node) {
		AVLNode leftTemp = node.left;

		node.left = leftTemp.right;
		leftTemp.right = node;

		node.height = 1 + Math.max(
				getHeight(node.left),
				getHeight(node.right)
		);
		leftTemp.height = 1 + Math.max(
				getHeight(leftTemp.left),
				getHeight(leftTemp.right)
		);

		return leftTemp;
	}

	private AVLNode rotateLeft(AVLNode node) {
		AVLNode rightTemp = node.right;

		node.right = rightTemp.left;
		rightTemp.left = node;

		node.height = 1 + Math.max(
				getHeight(node.left),
				getHeight(node.right)
		);
		rightTemp.height = 1 + Math.max(
				getHeight(rightTemp.left),
				getHeight(rightTemp.right)
		);

		return rightTemp;
	}

	private AVLNode rotateLeftRight(AVLNode node) {
		node.left = rotateLeft(node.left);

		return rotateRight(node);
	}

	private AVLNode rotateRightLeft(AVLNode node) {
		node.right = rotateRight(node.right);

		return rotateLeft(node);
	}

	private int getBalance(AVLNode node) {
		if (node == null) {
			return 0;
		}

		return getHeight(node.left) - getHeight(node.right);
	}

	private int getHeight(AVLNode node) {
		if (node == null) {
			return -1;
		}

		return node.height;
	}

	private TreeNode convertAvlNodesToTreenode(AVLNode avlNode) {
		if (avlNode == null) {
			return null;
		}

		TreeNode root = new TreeNode(avlNode.val);

		root.left = convertAvlNodesToTreenode(avlNode.left);
		root.right = convertAvlNodesToTreenode(avlNode.right);

		return root;
	}