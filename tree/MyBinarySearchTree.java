
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyBinarySearchTree implements BinarySearchTree {


	/**
	 * Root of the binary search tree.
	 */
	protected BinaryTreeNode treeRoot;

	/**
	 * Number of elements stored in the binary search tree.
	 */
	protected int treeSize;

	/**
	 * Initialization of the binary search tree.
	 */
	public MyBinarySearchTree() {
		treeRoot = null;
		treeSize = 0;
	}

	public MyBinarySearchTree(BinaryTreeNode root, int size) {
		treeRoot = root;
		treeSize = size;
	}

	@Override
	public BinaryTreeNode getRoot() {
		return this.treeRoot;
	}

	@Override
	public void insert(Integer key, String elem) throws IllegalArgumentException {
		if (key == null || elem == null) throw new IllegalArgumentException();

		BinaryTreeNode newNode = new BinaryTreeNode(key, elem);
		if (!insert(treeRoot, newNode)) {
			treeRoot = newNode;
			treeSize++;
		}

	}

	@Override
	public String find(Integer key) throws IllegalArgumentException {
		if (key == null) {
			throw new IllegalArgumentException();
		}
		if (find(key, getRoot()) != null) {
			return find(key, treeRoot).elem;
		}
		return null;
	}

	@Override
	public boolean remove(Integer key) throws IllegalArgumentException {

		if (key == null) {
			throw new IllegalArgumentException();
		}

		BinaryTreeNode toBeDeleted = find(key, treeRoot);

		// if key is not present
		if (toBeDeleted == null) {
			return false;

			// key is present search and kill
		} else {

			if (toBeDeleted == getRoot()) {
				return case3(toBeDeleted);

			} else {
				BinaryTreeNode parent = toBeDeleted.parent;

				// Case1 node has no children thus it can be removed
				if (toBeDeleted.left == null && toBeDeleted.right == null) {
					// find side to be set null
					if (parent.left == toBeDeleted) {
						parent.left = null;
					} else if (parent.right == toBeDeleted) {
						parent.right = null;
					}
					treeSize--;
					return true;
				}

				// Case2 node has only one child node
				else if (toBeDeleted.left == null || toBeDeleted.right == null) {

					if (toBeDeleted.left == null) {
						if (parent.left == toBeDeleted) {
							parent.left = toBeDeleted.right;
						} else if (parent.right == toBeDeleted) {
							parent.right = toBeDeleted.right;
						}
					} else {
						if (parent.left == toBeDeleted) {
							parent.left = toBeDeleted.left;
						} else if (parent.right == toBeDeleted) {
							parent.right = toBeDeleted.left;
						}
					}
					treeSize--;
					return true;
				}

				// Case3 node has two child nodes
				else {
					return case3(toBeDeleted);
				}
			}
		}
	}

	@Override
	public int size() {
		return treeSize;
	}

	@Override
	public Object[] toArrayPostOrder() {
		Object[] arr = new String[treeSize];
		return recursivePostOrder(arr, treeRoot, -1);
	}

	@Override
	public Object[] toArrayInOrder() {
		Object[] arr = new String[treeSize];
		return recursiveInOrder(arr, treeRoot, -1);
	}

	@Override
	public Object[] toArrayPreOrder() {
		Object[] arr = new String[treeSize];
		return recursivePreOrder(arr, treeRoot, -1);
	}

	@Override
	public Integer getParent(Integer key) throws IllegalArgumentException {
		if (key != null) {
			BinaryTreeNode node = find(key, treeRoot);
			if (node == null) {
				return null;
			} else if (node == treeRoot) {
				return treeRoot.key;
			} else {
				return node.parent.key;
			}
		} else {
			throw new IllegalArgumentException("No parent as nodeKey :" + key + " is root");
		}
	}

	@Override
	public boolean isRoot(Integer key) throws IllegalArgumentException {
		return getRoot().key.equals(key);
	}

	@Override
	public boolean isInternal(Integer key) throws IllegalArgumentException {
		if (key == null) {
			throw new IllegalArgumentException();
		} else {
			BinaryTreeNode node = find(key, treeRoot);
			return node.right != null || node.left != null;
		}
	}

	@Override
	public boolean isExternal(Integer key) throws IllegalArgumentException {
		if (key == null) {
			throw new IllegalArgumentException();
		} else{
			BinaryTreeNode node = find(key, treeRoot);
		return node.right == null && node.left == null;
		}
	}

	@Override
	public int[] runtimeComparison(ArrayList<Integer> list, int key) throws IllegalArgumentException {
		int[] nanoTimes = new int[2];
		int index = 0;

		// Test a list
		long startTimeList = System.nanoTime();
		for (Iterator<Integer> iterator = list.iterator(); iterator.hasNext(); ) {
			int i = iterator.next();
			if (i == key) {
				nanoTimes[1] = index;
				break;
			}
			index++;
		}
		long timeList = System.nanoTime() - startTimeList;
		System.out.println("list " + timeList + "ns");


		// Test the tree
		long startTimeTree = System.nanoTime();
		find(key);
		long timeBST = System.nanoTime() - startTimeTree;
		System.out.println("tree " + timeBST + "ns");

		long compare = timeList / timeBST;

		System.out.println("BST is " + compare  + "x faster than list.");

		nanoTimes[0] = maxDepth(getRoot());

		return nanoTimes;
	}

	public boolean isBST(BinarySearchTree bst) throws IllegalArgumentException {
		if (bst != null) {
			return isBST(bst.getRoot());
		} else {
			throw new IllegalArgumentException();
		}
	}

	public String returnMinKey() {
		if (getRoot() != null) {
			BinaryTreeNode node = getRoot();
			while (node.left != null) {
				node = node.left;
			}
			return node.key.toString();
		}
		return "BinaryTree is empty";
	}

	public int getDepth(Integer key) throws IllegalArgumentException {

		if (getRoot() != null) {                    // empty tree check
			BinaryTreeNode node = getRoot();
			int depth = 0;
			while (!node.key.equals(key)) {
				if (key > node.key) {
					node = node.right;
				} else {                        // if(key < node.key)
					node = node.left;
				}
				depth++;
			}
			return depth;
		}
		throw new IllegalArgumentException("BinaryTree is empty");
	}

	public boolean isTreeComplete() {
		if (treeRoot == null) {
			return false;
		}
		int compareDepth = 1;
		BinaryTreeNode node = treeRoot;
		// get depth
		while (node.left != null) {
			node = node.left;
			compareDepth++;
		}
		return testLeaves(compareDepth, false, treeRoot);
	}

	//------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------
	//----- PRIVATE METHODS --------------------------------------------------------------------
	//------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------

	private boolean insert(BinaryTreeNode node, BinaryTreeNode x) {
		while (node != null) {
			int compare = node.key.compareTo(x.key);
			if (compare == 0) {
				return true;
			} else if (compare > 0) {
				if (node.left == null) {
					node.left = x;
					x.parent = node;
					treeSize++;
					return true;
				} else
					node = node.left;
			} else {
				if (node.right == null) {
					node.right = x;
					x.parent = node;
					treeSize++;
					return true;
				} else
					node = node.right;
			}
		}
		return false;
	}

	private boolean testLeaves(final int depth, boolean isComplete, BinaryTreeNode node) {

		if (node == null) {
			return isComplete;
		}

		if (isLeaf(node)) {
			isComplete = getDepth(node.key) == depth;
		}

		if (isComplete) {
			isComplete = testLeaves(depth, true, node.left);
		}

		if (isComplete) {
			isComplete = testLeaves(depth, true, node.right);
		}

		return isComplete;
	}

	private boolean isLeaf(BinaryTreeNode node) {
		return node.left == null && node.right == null;
	}

	private boolean isBST(BinaryTreeNode node) {
		// left side value should be of lower key-value
		if (node.left != null) {
			if (node.left.key < node.key) {
				return isBST(node.left);
			} else {
				return false;
			}
		}
		// right side value should be of higher key-value
		if (node.right != null) {
			if (node.right.key > node.key) {
				return isBST(node.right);
			} else {
				return false;
			}
		}
		return true;
	}

	private int maxDepth(BinaryTreeNode node) {
		int leftDepth;
		int rightDepth;

		if (node == null) {
			return 0;
		}

		leftDepth = maxDepth(node.left);
		rightDepth = maxDepth(node.right);

		return leftDepth < rightDepth ? rightDepth + 1 : leftDepth + 1;
	}

	private Object[] recursiveInOrder(Object[] arr, BinaryTreeNode node, int arrPos) {
		if (node != null) {
			arr = recursiveInOrder(arr, node.left, arrPos);
			arrPos++;
			arr[arrPos] = node.elem;
			arr = recursiveInOrder(arr, node.right, arrPos);
		}
		return arr;
	}

	private Object[] recursivePostOrder(Object[] arr, BinaryTreeNode node, int arrPos) {
		if (node != null) {
			arr = recursivePostOrder(arr, node.left, arrPos);
			arr = recursivePostOrder(arr, node.right, arrPos);
			arrPos++;
			arr[arrPos] = node.elem;
		}
		return arr;
	}

	private Object[] recursivePreOrder(Object[] arr, BinaryTreeNode node, int arrPos) {
		if (node != null) {
			arrPos++;
			arr[arrPos] = node.elem;
			arr = recursivePreOrder(arr, node.left, arrPos);
			arr = recursivePreOrder(arr, node.right, arrPos);
		}
		return arr;
	}

	private boolean case3(BinaryTreeNode toBeDeleted) {
		List<BinaryTreeNode> treeNodes = new ArrayList<>();
		treeNodes = getTreeNodes(toBeDeleted);

		for (BinaryTreeNode n : treeNodes) {
			if (n != toBeDeleted) {
				insert(n.key, n.elem);
			}
		}
		treeSize--;
		return true;
	}

	private List<BinaryTreeNode> getTreeNodes(BinaryTreeNode node) {
		List<BinaryTreeNode> list = new ArrayList<>();
		if (node != null) {
			list.add(node);
			list.addAll(getTreeNodes(node.left));
			list.addAll(getTreeNodes(node.right));
		}
		return list;
	}

	// recursive search
	private BinaryTreeNode find(Integer key, BinaryTreeNode node) {

		// reached end of current branch
		if (node == null) {
			return null;
		}

		// found
		if (node.key.equals(key)) {
			return node;
		}

		// next recursion step
		if (key < node.key) {
			return find(key, node.left);
		} else {
			return find(key, node.right);
		}

	}
}