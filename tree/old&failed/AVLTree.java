
public class AVLTree implements AVLTreeIF {

	private AVLNode root;
	private int size;

	// cut & merge indices
	private static final int T0 = 0, A = 1, T1 = 2, B = 3, T2 = 4, C = 5, T3 = 6;
	AVLNode[] avlArr = new AVLNode[7];


	/**
	 * Default constructor. Initializes the AVL tree.
	 */
	public AVLTree() {
		root = null;
		size = 0;
	}

	/**
	 * @returns the root of the AVLTree
	 */
	public AVLNode getRoot() {
		return root;
	}
	
	/**
	 * Retrieves tree height.
	 * 
	 * @return -1 in case of empty tree, current tree height otherwise.
	 */
	public int height() {

		//empty tree
		if (root == null) {
			return -1;
		}

		// just one node
		if (root.left == null && root.right == null) {
			return 0;
		}

		return root.height;
	}

	/**
	 * Yields number of key/element pairs in the tree.
	 * 
	 * @return Number of key/element pairs.
	 */
	public int size() {
		return size;
	}

	/**
	 * Yields an array representation of the tree elements (pre-order).
	 * 
	 * @return Array representation of the tree elements.
	 */
	public Float[] toArray() {
		Float[] arr = new Float[size];
		return recursivePreOrder(arr, root, -1);
	}

	/**
	 * Returns element of node with given key.
	 * 
	 * @param key
	 *            Key to search.
	 * @return 
	 *            Corresponding element if key was found, null otherwise.
	 * @throws 
	 *            IllegalArgumentException if key is null.
	 */
	public Float find(Integer key) throws IllegalArgumentException {
		if (key == null) {
			throw new IllegalArgumentException();
		}

		AVLNode searched = findKey(key, root);

		if (searched != null) {
			return searched.elem;
		}

		return null;
	}

	/**
	 * Inserts a new node into AVL tree.
	 * 
	 * @param key
	 *            Key of the new node.
	 * @param elem
	 *            Data of the new node. Elements with the same key
	 *            are not allowed. In this case false is returned. Null-Keys are
	 *            not allowed. In this case an exception is thrown.
	 * @throws 
	 *            IllegalArgumentException if one of the parameters is null.
	 */
	public boolean insert(Integer key, Float elem) throws IllegalArgumentException {
		if (key == null || elem == null) {
			throw new IllegalArgumentException();
		}

		// check for doubles
		if (findKey(key, root) != null) {
			return false;
		}

		AVLNode newAvlNode = new AVLNode(key, elem);

		if (root == null) {
			root = newAvlNode;
			size++;
			return true;
		}

		boolean flag = insert(root, newAvlNode);        // private recursive Method
		if (!flag) return false;                        // break here if it wasn't successful

		updateHeight(newAvlNode);

		// check balances if insert breaks balance at this side.
		AVLNode curr = newAvlNode;
		while (curr.parent != null && curr.parent.parent != null && Math.abs(getBalance(curr.parent)) <= 1) {
			curr = curr.parent;
		}

		// do one reconstruct if needed
		if (curr != null) {
			reconstruct(curr);
		}

		return true;
	}


	/**
	 * Removes node with given key.
	 *
	 * @param key
	 *            Key of node to remove.
	 * @return
	 *            true If element was found and deleted.
	 * @throws
	 *            IllegalArgumentException if key is null.
	 */
	public boolean remove(Integer key) throws IllegalArgumentException {
		// ToDo
		return false;
	}

	//------------------------------------------------------------------------------------------
	//----- PRIVATE AUXILIARY METHODS ----------------------------------------------------------
	//------------------------------------------------------------------------------------------

	private void reconstruct(AVLNode child) {

		if (child == null || child.parent == null || child.parent.parent == null) {
			return; // check Error-cases
		}

		AVLNode father = child.parent;
		AVLNode grandfather = child.parent.parent;

		AVLNode[] abc = getABC(child.parent.parent, child.parent, child);

		avlArr[A] = abc[0];
		avlArr[B] = abc[1];
		avlArr[C] = abc[2];
		avlArr[T0] = avlArr[A].left;
		avlArr[T3] = avlArr[C].right;

		if (avlArr[B] == father) {
			if (child.key < grandfather.key) {
				// Right Rotation
				avlArr[T1] = avlArr[A].right;
				avlArr[T2] = avlArr[B].right;
			} else {
				// Left Rotation
				avlArr[T1] = avlArr[B].left;
				avlArr[T2] = avlArr[C].left;
			}
		} else {
			// LeftRight RightLeft
			avlArr[T1] = child.left;
			avlArr[T2] = child.right;
		}

		// cut & merge

		AVLNode aFather = grandfather.parent;
		if(avlArr[A] == root){
			avlArr[B] = root;
			avlArr[B].parent = null;
		}else /*(avlArr[A] != root)*/{
			if(aFather.right == avlArr[A]){
				aFather.right = avlArr[B];
			}else{
				aFather.left = avlArr[B];
			}
			avlArr[B].parent = aFather;
		}

		avlArr[B].left = avlArr[A];
		avlArr[A].parent = avlArr[B];

		avlArr[B].right = avlArr[C];
		avlArr[C].parent = avlArr[B];


		if (avlArr[T0] != null) {
			avlArr[A].left = avlArr[T0];
			avlArr[T0].parent = avlArr[A];
		} else {
			avlArr[A].left = null;
		}

		if (avlArr[T1] != null) {
			avlArr[A].right = avlArr[T1];
			avlArr[T1].parent = avlArr[A];
		} else {
			avlArr[A].right = null;
		}

		if (avlArr[T2] != null) {
			avlArr[C].left = avlArr[T2];
			avlArr[T2].parent = avlArr[C];
		} else {
			avlArr[C].left = null;
		}

		if (avlArr[T3] != null) {
			avlArr[C].right = avlArr[T3];
			avlArr[T3].parent = avlArr[C];
		} else {
			avlArr[C].right = null;
		}

		root.parent = null;

		// UpdateHeights

		avlArr[A].height = Math.max(getHeight(avlArr[A].right), getHeight(avlArr[A].left)) + 1;
		avlArr[B].height = Math.max(getHeight(avlArr[B].right), getHeight(avlArr[B].left)) + 1;
		avlArr[C].height = Math.max(getHeight(avlArr[C].right), getHeight(avlArr[C].left)) + 1;
		updateHeight(avlArr[B]);

		// clear Array
		avlArr = new AVLNode[7];
	}

	private AVLNode[] getABC(AVLNode x, AVLNode y, AVLNode z) {
		AVLNode[] solution;
		int xKey = x.key, yKey = y.key, zKey = z.key;

		// Right side
		if (xKey > yKey) {
			if (xKey > zKey) {
				if (yKey > zKey) {
					return new AVLNode[]{z, y, x};
				} else {
					return new AVLNode[]{y, z, x};
				}
			} else {
				return new AVLNode[]{y, x, z};
			}
			// Left side
		} else {
			if (zKey < yKey) {
				if (zKey < xKey) {
					return new AVLNode[]{z, x, y};
				} else {
					return new AVLNode[]{x, z, y};
				}
			}
			return new AVLNode[]{x, y, z};
		}
	}

	// returns the balance of given nodes children (r-l)
	private int getBalance(AVLNode avlNode) {
		return getHeight(avlNode.right) - getHeight(avlNode.left);
	}

	// this will return the height of a given node
	private int getHeight(AVLNode avlNode) {
		if (avlNode == null) {
			return -1;
		}
		return avlNode.height;
	}

	// for returning tree as array
	private Float[] recursivePreOrder(Float[] arr, AVLNode avlNode, int arrPos) {
		if (avlNode != null) {
			arrPos++;
			arr[arrPos] = avlNode.elem;
			arr = recursivePreOrder(arr, avlNode.left, arrPos);
			arr = recursivePreOrder(arr, avlNode.right, arrPos);
		}
		return arr;
	}

	// recursive insert
	private boolean insert(AVLNode curr, AVLNode newAvlNode) {
		boolean flag = false;
		// add left key < curr.key
		if (newAvlNode.key < curr.key) {
			if (curr.left != null) {
				flag = insert(curr.left, newAvlNode);
			} else {
				curr.left = newAvlNode;
				newAvlNode.parent = curr;
				size++;
				return true;
			}
			// add right key > curr.key
		} else if(newAvlNode.key > curr.key) {
			if (curr.right != null) {
				flag = insert(curr.right, newAvlNode);
			} else {
				curr.right = newAvlNode;
				newAvlNode.parent = curr;
				size++;
				return true;
			}
		}else /*(right key == curr.key) */ {
			// Error should not be reached.
			flag = false;
		}
		return flag;
	}

	// after insert update heights
	public void updateHeight(AVLNode avlNode) {
		if (avlNode != null) {
			avlNode.height = Math.max(getHeight(avlNode.left), getHeight(avlNode.right)) + 1;
			if (avlNode.parent != null) {
				updateHeight(avlNode.parent);
			}
		}
	}


	// check if key is already there
	private AVLNode findKey(Integer key, AVLNode avlNode) {
		// reached end of current branch
		if (avlNode == null) {
			return null;
		}
		// found
		if (avlNode.key.equals(key)) {
			return avlNode;
		}
		// next recursion step
		if (key < avlNode.key) {
			return findKey(key, avlNode.left);
		} else {
			return findKey(key, avlNode.right);
		}
	}
}
