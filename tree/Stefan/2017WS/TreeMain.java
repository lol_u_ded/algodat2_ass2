public class TreeMain {

	public static void main(String[] args) {
		final AVLTree t = new AVLTree();
		for (int i = 100; i < 110; i++) {
			t.insert(i, String.valueOf(i));
		}
		t.insert(99, "");
		System.out.println(t.toString());
		t.remove(100);
		System.out.println(t.toString());
		t.remove(102);
		System.out.println(t.toString());
		t.remove(99);
		System.out.println(t.toString());
		t.remove(108);
		System.out.println(t.toString());
	}

}
