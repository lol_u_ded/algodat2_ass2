import java.util.Arrays;
import java.util.Comparator;

public class AVLTree {

	protected AVLNode root;
	protected int size;
	protected AVLNode errorNode;

	/**
	 * Default constructor.
	 * Initializes the AVL tree.
	 */
	public AVLTree() {
		root = null;
		size = 0;
		errorNode = null;
	}

	/**
	 * Yields number of key/value pairs in the tree. * @return Number of key/value pairs.
	 */
	public int size() {
		return size;
	}

	/**
	 * Retrieves tree height.
	 *
	 * @return -1 in case of empty tree, current tree height otherwise.
	 */
	public int height() {
		return root != null ? root.height : -1;
	}

	/**
	 * Inserts a new node into AVL tree.
	 *
	 * @param key  Key of the new node.
	 * @param data Data of the new node. May be null.
	 */
	public void insert(Integer key, String data) throws IllegalArgumentException {
		if (key == null) throw new IllegalArgumentException();

		if (root == null) {
			root = new AVLNode(key, data);
			size = 1;
		} else {
			AVLNode curr = root, prev = null;
			while (curr != null) {
				prev = curr;
				if (key.compareTo(curr.key) < 0) {
					curr = curr.left;
				} else if (key.compareTo(curr.key) > 0) {
					curr = curr.right;
				} else {
					break;
				}
			}
			if (curr != null && curr.key.equals(key)) return;
			final AVLNode inserted = new AVLNode(key);
			inserted.parent = prev;
			if (inserted.key.compareTo(prev.key) < 0) {
				prev.left = inserted;
			} else {
				prev.right = inserted;
			}
			updateHeightsUpward(inserted);

			// Ersten unbalancierten Grandparent suchen, falls vorhanden
			rebalanceUpward(inserted);
			size++;
		}
	}

	private boolean isBalancedGrandChild(AVLNode node) {
		return node.parent != null && node.parent.parent != null
			&& Math.abs(height(node.parent.parent.left) - height(node.parent.parent.right)) <= 1;
	}

	private void rebalanceUpward(AVLNode curr) {
		while (curr != root) {
			while (isBalancedGrandChild(curr)) {
				curr = curr.parent;
			}
			// insert ist nicht null zu diesem Zeitpunkt
			// falls es eine Unbalanciertheit gibt, stehen wir nun auf ihr
			// falls nucht, passiert nichts
			if (curr.parent != null && curr.parent.parent != null) {
				restructure(curr);
			} else {
				curr = curr.parent;
			}
		}
	}

	/**
	 * Removes the first node with given key.
	 *
	 * @param key Key of node to remove.
	 * @return true If element was found and deleted.
	 */
	public boolean remove(Integer key) throws IllegalArgumentException {
		if (key == null) throw new IllegalArgumentException();

		// Root löschen
		if (root.key.compareTo(key) == 0) {
			size--;
			// Fall 1: Root hat keinen Kindknoten; Knoten kann einfach entfernt werden / geht
			if (root.left == null && root.right == null) {
				isolate(root);
				return true;
			}
			// Fall 2: Root hat nur einen Kindknoten; Knoten durch Kindknoten ersetzen / geht
			if (root.right == null) {
				root = root.left;
				root.parent = null;
				root.height--;
				restructure(root);
				return true;
			} else if (root.left == null) {
				root = root.right;
				root.parent = null;
				root.height--;
				restructure(root);
				return true;
			}
			// Fall 3: Root hat zwei Kindknoten / geht
			final AVLNode successor = inOrderSuccessor(root);
			root.key = successor.key;
			root.data = successor.data;
			if (successor.parent != root) {
				successor.parent.left = successor.right;
				if (successor.right != null) successor.right.parent = successor.parent;
			} else {
				root.right = successor.right;
				if (successor.right != null) {
					successor.right.parent = root;
				}
			}
			successor.right = null;
			successor.parent = null;
			root.height = Math.max(root.left.height, root.right.height) + 1;
			return true;
		}

		// Nicht die Root löschen
		final AVLNode node = find(key, root);
		// Key nicht im Baum -> false
		if (node == null) return false;

		size--;
		// Fall 1: Knoten hat keinen Kindknoten; Knoten kann einfach entfernt werden / geht
		// noch am Arbeiten
		final AVLNode toTest, parent;
		AVLNode curr;
		if (node.left == null && node.right == null) {
			// Wenn der zu entfernende Knoten links steht, dann links die referenz null setzen
			if (node.parent.left == node) {
				node.parent.left = null;
			} else {
				node.parent.right = null;
			}
			curr = node.parent;
			isolate(node);
			updateHeightsUpward(curr);
		} else if (node.left == null) {
			if (node.parent.left == node) {
				node.parent.left = node.right;
			} else {
				node.parent.right = node.right;
			}
			parent = node.parent;
			node.right.parent = parent;
			curr = parent;
			isolate(node);
			updateHeightsUpward(curr);
		} else if (node.right == null) {
			parent = node.parent;
			if (parent.left == node) {
				parent.left = node.left;
			} else {
				parent.right = node.left;
			}
			node.left.parent = parent;
			isolate(node);
			curr = parent;
			updateHeightsUpward(curr);
		} else {
			AVLNode successor = inOrderSuccessor(node);
			node.key = successor.key;
			node.data = successor.data;
			if (successor.parent != node) {
				successor.parent.left = successor.right;
				if (successor.right != null) successor.right.parent = successor.parent;
			} else {
				node.right = successor.right;
				if (successor.right != null) {
					successor.right.parent = node;
				}
			}
			curr = successor.parent;
			isolate(successor);
			updateHeightsUpward(curr);
			updateHeightsUpward(node);
			rebalanceUpward(curr);
			// check other side too
			curr = node;
		}
		rebalanceUpward(curr);

		// as long as there IS an error - correct it.
		// TODO könnte der Scanner auch nicht lieb haben
		while (!isAVLTree(root)) {
			findAndCorrect(errorNode);
		}
		return true;
	}

	private void isolate(AVLNode node) {
		node.parent = node.left = node.right = null;
		// TODO debug: Absichtliches Fehlerproduzieren, falls doch noch eine Referenz besteht
		node.height = Integer.MAX_VALUE;
		node.data = null;
		node.key = null;
	}

	/**
	 * Implements cut & link restructure operation. * @param n Node to start restructuring with.
	 */
	private void restructure(AVLNode grandChild) {
		if (grandChild == null) return;
		final AVLNode child = grandChild.parent, parent = child.parent;
		// Drei Basisknoten sortieren
		final AVLNode[] sort = new AVLNode[]{parent, child, grandChild};
		Arrays.sort(sort, Comparator.comparingInt(node -> node.key));

		// Alle 7 Knoten sortieren
		final NodeGroup nodes = new NodeGroup();
		nodes.first = sort[0].left;
		nodes.new_left = sort[0];
		nodes.new_parent = sort[1];
		nodes.new_right = sort[2];
		nodes.fourth = sort[2].right;
		if (nodes.new_parent != child) {
			nodes.second = grandChild.left;
			nodes.third = grandChild.right;
		} else {
			if (grandChild.key < parent.key) {
				nodes.second = nodes.new_left.right;
				nodes.third = nodes.new_parent.right;
			} else {
				nodes.second = nodes.new_parent.left;
				nodes.third = nodes.new_right.left;
			}
		}

		// Cut & Link
		final AVLNode grandparent = parent.parent;
		if (root == parent || grandparent == null) {
			root = nodes.new_parent;
		}
		nodes.new_parent.left = nodes.new_left;
		nodes.new_left.parent = nodes.new_parent;
		nodes.new_parent.right = nodes.new_right;
		nodes.new_right.parent = nodes.new_parent;

		nodes.new_left.left = nodes.first;
		if (nodes.first != null) nodes.first.parent = nodes.new_left;
		nodes.new_left.right = nodes.second;
		if (nodes.second != null) nodes.second.parent = nodes.new_left;

		nodes.new_right.left = nodes.third;
		if (nodes.third != null) nodes.third.parent = nodes.new_right;
		nodes.new_right.right = nodes.fourth;
		if (nodes.fourth != null) nodes.fourth.parent = nodes.new_right;

		nodes.new_parent.parent = grandparent;
		if (grandparent != null) {
			if (grandparent.left == parent) {
				grandparent.left = nodes.new_parent;
			} else {
				grandparent.right = nodes.new_parent;
			}
		}

		// Höhen neu berechnen
		setNewHeight(nodes.new_left);
		setNewHeight(nodes.new_right);
		// den höchsten der drei neuen Knoten zum schluss, damit er schon mit den neuen Höhen rechnen kann
		updateHeightsUpward(nodes.new_parent);
	}

	private int height(AVLNode node) {
		return node != null ? node.height : -1;
	}

	private void setNewHeight(AVLNode node) {
		node.height = Math.max(height(node.left), height(node.right)) + 1;
	}

	/**
	 * Updates node heights starting from given node.
	 *
	 * @param curr Node to start update height operation with.
	 */
	private void updateHeightsUpward(AVLNode curr) {
		while (curr != null) {
			setNewHeight(curr);
			curr = curr.parent;
		}
	}

	/**
	 * Checks AVL integrity.
	 *
	 * @param n Node to check integrity for.
	 * @return true If AVL integrity is sane, false otherwise.
	 */
	private boolean isAVLTree(AVLNode n) {
		if (n == null) return true;

		final int right = n.right != null ? n.right.height : -1;
		final int left = n.left != null ? n.left.height : -1;
		if (Math.abs(right - left) >= 2) {
			errorNode = n;
			return false;
		}
		return isAVLTree(n.left) && isAVLTree(n.right);
	}

	private void findAndCorrect(AVLNode n) {
		if (n == null) return;

		if (Math.abs(height(n.left) - height(n.right)) > 1) {
			AVLNode grandchild = null;
			final AVLNode child = height(n.left) >= height(n.right) ? n.left : n.right;
			if (child != null) {
				grandchild = height(child.left) >= height(child.right) ? child.left : child.right;
			}
			restructure(grandchild);
			errorNode = null;
		}
	}

	/**
	 * Returns value of a first found node with given key.
	 *
	 * @param key Key to search.
	 * @return Corresponding value if key was found, null otherwise.
	 */
	public String find(Integer key) throws IllegalArgumentException {
		if (key == null) throw new IllegalArgumentException();

		final AVLNode result = find(key, root);
		return result != null && result.key.equals(key) ? result.data : null;
	}

	private AVLNode find(Integer key, AVLNode curr) {
		while (curr != null) {
			if (key.compareTo(curr.key) < 0) {
				curr = curr.left;
			} else if (key.compareTo(curr.key) > 0) {
				curr = curr.right;
			} else {
				return curr;
			}
		}
		return null;
	}

	private AVLNode inOrderSuccessor(AVLNode node) {
		if (node.right != null) {
			AVLNode successor = node.right;
			while (successor.left != null) {
				successor = successor.left;
			}
			return successor;
		}

		AVLNode parent = node.parent;
		while (parent != null && node == parent.right) {
			node = parent;
			parent = parent.parent;
		}
		return parent;
	}

	/**
	 * Yields an array representation of the data elements (in-order). * @return Array representation of the tree.
	 */
	public Object[] toArray() {
		final AVLNode[] array = new AVLNode[size];
		toArrayInOrder(array, new MyIndex(0), root);
		return array;
	}

	private void toArrayInOrder(AVLNode[] order, MyIndex idx, AVLNode curr) {
		if (curr != null) {
			toArrayInOrder(order, idx, curr.left);
			order[idx.getAndIncrement()] = curr;
			toArrayInOrder(order, idx, curr.right);
		}
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		toString(root, new StringBuilder(), true, sb);
		return sb.toString();
	}

	private void toString(AVLNode curr, StringBuilder prefix, boolean down, StringBuilder sb) {
		if (curr == null) return;

		if (curr.right != null) {
			toString(curr.right, new StringBuilder().append(prefix).append(down ? "│   " : "    "), false,
				sb);
		}
		sb.append(prefix).append(down ? "└── " : "┌── ").append(curr.key).append("<").append(curr.height).append(">").append("\n");
		if (curr.left != null) {
			toString(curr.left, new StringBuilder().append(prefix).append(down ? "    " : "│   "), true,
				sb);
		}
	}

	private static class MyIndex {
		private int val;

		private MyIndex(int initialValue) {
			this.val = initialValue;
		}

		private int getAndIncrement() {
			return val++;
		}
	}

	private static class NodeGroup {
		private AVLNode new_left, new_right, new_parent;
		private AVLNode first, second, third, fourth;
	}

}
