public class AVLNode {

	public AVLNode parent = null;
	public AVLNode left = null;
	public AVLNode right = null;

	public Integer key;
	public String data;

	public int height = 0;

	public AVLNode(Integer key, String elem) {
		this.key = key;
		this.data = elem;
	}

	public AVLNode(Integer key) {
		this.key = key;
	}

}