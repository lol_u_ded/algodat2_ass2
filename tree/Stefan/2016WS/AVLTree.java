import java.util.concurrent.atomic.AtomicInteger;


public class AVLTree {
	
	// wozu protected?
	protected AVLNode				root;
	protected int					size;
	
	private final AVLNode[]		nodes	= new AVLNode[7];
	private static final int	T0		= 0, T1 = 2, T2 = 4, T3 = 6, A = 1, B = 3, C = 5;
	
	/**
	 * Default constructor. Initializes the AVL tree.
	 */
	public AVLTree() {
		root = null;
		size = 0;
	}
	
	/**
	 * Yields number of key/value pairs in the tree.
	 * 
	 * @return Number of key/value pairs.
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Retrieves tree height.
	 * 
	 * @return -1 in case of empty tree, current tree height otherwise.
	 */
	public int height() {
		return root != null ? root.height : -1;
	}
	
	/**
	 * Inserts a new node into AVL tree.
	 * 
	 * @param key
	 *           Key of the new node.
	 * @param data
	 *           Data of the new node. May be null.
	 */
	public void insert(Long key, Integer data) throws IllegalArgumentException {
		if (key == null) throw new IllegalArgumentException("Cannot insert with a null Key");
		
		if (root == null) {
			root = new AVLNode(key, data);
		} else {
			AVLNode curr = root, toInsert = new AVLNode(key, data);
			while (curr != null) {
				if (curr.key.compareTo(toInsert.key) == 0) {
					System.out.println("Cannot insert duplicate key");
					return;
				}
				if (toInsert.key.compareTo(curr.key) < 0) {
					if (curr.left == null) break;
					curr = curr.left;
				} else {
					if (curr.right == null) break;
					curr = curr.right;
				}
			}
			// now current has no children
			
			if (toInsert.key.compareTo(curr.key) < 0) {
				curr.left = toInsert;
				toInsert.parent = curr;
			} else {
				curr.right = toInsert;
				toInsert.parent = curr;
			}
			updateHeightsOnInsert(toInsert);
			while (toInsert.parent != null && toInsert.parent.parent != null
					&& heigthDifference(toInsert.parent.parent.left,
							toInsert.parent.parent.right) <= 1) {
				toInsert = toInsert.parent;
			}
			
			// Beim Inserten wird der neue Knoten direkt mal in Ordnung gebracht,
			// falls dadurch ein Fehler entstehen wuerde
			if (toInsert != null) reconstruct(toInsert);
		}
		size++;
	}
	
	// public boolean test() {
	// return test(root);
	// }
	
	/**
	 * Updates node heights starting from given node.
	 * 
	 * @param n
	 *           Node to start update height operation with.
	 */
	private void updateHeightsOnInsert(AVLNode current) {
		current = current.parent;
		while (current != null) {
			current.height = Math.max(height(current.left), height(current.right)) + 1;
			current = current.parent;
		}
	}
	
	private void reconstruct(AVLNode grandchild) {
		for (int i = 0; i < nodes.length; i++) {
			nodes[i] = null;
		}
		if (grandchild == null || grandchild.parent == null || grandchild.parent.parent == null)
			return;
		final AVLNode child = grandchild.parent
				, parent = child.parent;
		
		final AVLNode grandparent = parent.parent;
		indexNodes(parent, child, grandchild);
		cutAndLink(parent, grandparent);
		
		updateHeightOnRestruct();
	}
	
	private void indexNodes(AVLNode parent, AVLNode child, AVLNode grandchild) {
		final AVLNode[] abc = getABC(parent, child, grandchild);
		
		nodes[A] = abc[0];
		nodes[B] = abc[1];
		nodes[C] = abc[2];
		nodes[T0] = nodes[A].left;
		nodes[T3] = nodes[C].right;
		if (nodes[B] == child) {
			if (grandchild.key < parent.key) {
				nodes[T1] = nodes[A].right;
				nodes[T2] = nodes[B].right;
			} else {
				nodes[T1] = nodes[B].left;
				nodes[T2] = nodes[C].left;
			}
		} else {
			nodes[T1] = grandchild.left;
			nodes[T2] = grandchild.right;
		}
	}
	
	/** Sorts three nodes accordingly to their keys */
	private static AVLNode[] getABC(AVLNode x, AVLNode y, AVLNode z) {
		if (x.key > y.key) {
			if (x.key > z.key) {
				if (y.key > z.key) {
					return new AVLNode[] { z, y, x };
				} else {
					return new AVLNode[] { y, z, x };
				}
			} else {
				return new AVLNode[] { y, x, z };
			}
		} else {
			if (y.key > z.key) {
				if (x.key > z.key) {
					return new AVLNode[] { z, x, y };
				} else {
					return new AVLNode[] { x, z, y };
				}
			} else {
				return new AVLNode[] { x, y, z };
			}
		}
	}
	
	private void cutAndLink(final AVLNode parent, final AVLNode grandparent) {
		if (root == parent || grandparent == null) {
			root = nodes[B];
		}
		
		nodes[B].left = nodes[A];
		nodes[A].parent = nodes[B];
		nodes[B].right = nodes[C];
		nodes[C].parent = nodes[B];
		
		nodes[A].left = nodes[T0];
		if (nodes[T0] != null) nodes[T0].parent = nodes[A];
		nodes[A].right = nodes[T1];
		if (nodes[T1] != null) nodes[T1].parent = nodes[A];
		
		nodes[C].left = nodes[T2];
		if (nodes[T2] != null) nodes[T2].parent = nodes[C];
		nodes[C].right = nodes[T3];
		if (nodes[T3] != null) nodes[T3].parent = nodes[C];
		
		nodes[B].parent = grandparent;
		if (grandparent != null) {
			if (grandparent.left == parent) {
				grandparent.left = nodes[B];
			} else {
				grandparent.right = nodes[B];
			}
		}
	}
	
	private void updateHeightOnRestruct() {
		nodes[A].height = Math.max(height(nodes[A].left), height(nodes[A].right)) + 1;
		nodes[C].height = Math.max(height(nodes[C].left), height(nodes[C].right)) + 1;
		nodes[B].height = Math.max(nodes[A].height, nodes[C].height) + 1;
		
		AVLNode curr = nodes[B].parent;
		while (curr != null) {
			curr.height = Math.max(height(curr.left), height(curr.right)) + 1;
			curr = curr.parent;
		}
	}
	
	/**
	 * Returns value of a first found node with given key.
	 * 
	 * @param key
	 *           Key to search.
	 * @return Corresponding value if key was found, null otherwise.
	 */
	public Integer find(Long key) throws IllegalArgumentException {
		if (key == null) throw new IllegalArgumentException("Cannot search for null key");
		
		final AVLNode ret = findNode(key);
		return ret != null ? ret.data : null;
	}
	
	private AVLNode findNode(Long key) {
		if (root == null || key == null) return null;
		
		AVLNode curr = root;
		while (curr != null) {
			if (key.compareTo(curr.key) == 0) return curr;
			
			if (key.compareTo(curr.key) < 0) curr = curr.left;
			else
				curr = curr.right;
		}
		return curr;
	}
	
	private AVLNode inOrderSuccessor(AVLNode node) {
		if (node == null) return null;
		
		AVLNode successor = null;
		if (node.right != null) {
			successor = node.right;
			while (successor.left != null) {
				successor = successor.left;
			}
			return successor;
		}
		
		AVLNode p = node.parent;
		while (p != null && node == p.right) {
			node = p;
			p = p.parent;
		}
		return p;
	}
	
	/**
	 * Removes the first node with given key.
	 * 
	 * @param key
	 *           Key of node to remove.
	 * @return true If element was found and deleted.
	 */
	public boolean remove(Long key) throws IllegalArgumentException {
		if (key == null) throw new IllegalArgumentException("Cannot search for null key");
		
		// tree has no content
		if (root == null) return false;
		
		AVLNode node = findNode(key);
		// Tree doesnt contain node
		if (node == null) return false;
		
		if (node.left == null && node.right == null && node != root) {
			if (node.parent.left == node) {
				node.parent.left = null;
			} else {
				node.parent.right = null;
			}
			AVLNode p = node.parent;
			node.parent = null;
			node = null;
			updateHeightsOnRemove(p);
		} else if (node.left == null && node.right == null && node == root) {
			root = null;
			node = null;
		} else if (node.left == null && node.right != null && node != root) {
			if (node.parent.left == node) {
				node.parent.left = node.right;
			} else {
				node.parent.right = node.right;
			}
			AVLNode p = node.right;
			node.right.parent = node.parent;
			node.parent = null;
			node.right = null;
			node = null;
			updateHeightsOnRemove(p);
		} else if (node.left == null && node.right != null && node == root) {
			root = node.right;
			root.parent = null;
			node.right = null;
			node = null;
			updateHeightsOnRemove(root);
		} else if (node.left != null && node.right == null && node != root) {
			if (node.parent.left == node) {
				node.parent.left = node.left;
			} else {
				node.parent.right = node.left;
			}
			AVLNode p = node.left;
			node.left.parent = node.parent;
			node.parent = null;
			node.left = null;
			node = null;
			updateHeightsOnRemove(p);
		} else if (node.left != null && node.right == null && node == root) {
			root = node.left;
			root.parent = null;
			node.left = null;
			node = null;
			updateHeightsOnRemove(root);
		} else if (node.left != null && node.right != null && node != root) {
			AVLNode successor = inOrderSuccessor(node);
			node.key = successor.key;
			node.data = successor.data;
			if (successor.parent != node) {
				successor.parent.left = successor.right;
				if (successor.right != null) successor.right.parent = successor.parent;
			} else {
				node.right = successor.right;
				if (successor.right != null) {
					successor.right.parent = node;
				}
			}
			successor.right = null;
			AVLNode p = successor.parent;
			successor.parent = null;
			successor = null;
			
			updateHeightsOnRemove(p);
			updateHeightsOnRemove(node);
		} else if (node.left != null && node.right != null && node == root) {
			AVLNode successor = inOrderSuccessor(node);
			node.key = successor.key;
			node.data = successor.data;
			if (successor.parent != node) {
				successor.parent.left = successor.right;
				if (successor.right != null) successor.right.parent = successor.parent;
			} else {
				node.right = successor.right;
				if (successor.right != null) {
					successor.right.parent = node;
				}
			}
			
			successor.right = null;
			AVLNode p = successor.parent;
			successor.parent = null;
			successor = null;
			
			updateHeightsOnRemove(p);
			updateHeightsOnRemove(node);
		}
		
		while (isNotAVLTree(root)) {}
		size--;
		return true;
	}
	
	private void updateHeightsOnRemove(AVLNode current) {
		if (current == null) { return; }
		while (current.parent != null) {
			current.height = Math.max(height(current.left), height(current.right)) + 1;
			current = current.parent;
		}
	}
	
	/**
	 * Yields an array representation of the tree (in-order).
	 * 
	 * @return Array representation of the tree.
	 */
	public Object[] toArray() {
		if (root == null) return null;
		
		final Object[] obj = new Object[size];
		if (size > 0) addToArrayInOrder(obj, root, new AtomicInteger(0));
		return obj;
	}
	
	private void addToArrayInOrder(final Object[] obj, final AVLNode node, final AtomicInteger i) {
		if (node.left == null && node.right == null) {
			obj[i.getAndIncrement()] = node.data;
			return;
		}
		
		if (node.left != null) addToArrayInOrder(obj, node.left, i);
		obj[i.getAndIncrement()] = node.data;
		if (node.right != null) addToArrayInOrder(obj, node.right, i);
	}
	
	private void findAndCorrect(AVLNode n) {
		if (n == null) return;
		
		if (heigthDifference(n.left, n.right) > 1) {
			AVLNode child = null, grandchild = null;
			if (height(n.left) >= height(n.right)) {
				child = n.left;
			} else {
				child = n.right;
			}
			if (child != null) {
				if (height(child.left) >= height(child.right)) {
					grandchild = child.left;
				} else {
					grandchild = child.right;
				}
			}
			reconstruct(grandchild);
		}
	}
	
	// private boolean test(AVLNode n) {
	// if (n == null) return true;
	//
	// if (heigthDifference(n.left, n.right) >= 2) {
	// return false;
	// } else {
	// return test(n.left) && test(n.right);
	// }
	// }
	
	private boolean isNotAVLTree(AVLNode n) {
		if (n == null) return false;
		
		if (heigthDifference(n.left, n.right) >= 2) {
			findAndCorrect(n);
			return true;
		} else {
			return isNotAVLTree(n.left) || isNotAVLTree(n.right);
		}
	}
	
	private static int height(AVLNode n1) {
		return n1 != null ? n1.height : -1;
	}
	
	private static int heigthDifference(AVLNode n1, AVLNode n2) {
		final int c = height(n1) - height(n2);
		return c >= 0 ? c : -c; // absolute value
	}
	
}
